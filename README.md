# Logitech Media Server
This is a helm chart for [Logitech Media Server][home].

## TL;DR:
```console
helm install incubator/logitech-media-server
```

## Introduction
This code is adopted from [this original repo][github].

## Installing the Chart
To install the chart with the release name `my-release`:

```console
helm install --name my-release incubator/logitech-media-server
```

## Uninstalling the Chart
To uninstall/delete the `my-release` deployment:

```console
helm delete my-release --purge
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration
The following tables lists the configurable parameters of the Logitech Media Server chart and their default values.

| Parameter                          | Description                                                                                                                         | Default                         |
| ---------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- | ------------------------------- |
| `image.repository`                 | Image repository                                                                                                                    | `doliana/logitech-media-server` |
| `image.tag`                        | Image tag. Possible values listed [here][docker].                                                                                   | `7.9.2_2019_06_21`              |
| `image.pullPolicy`                 | Image pull policy                                                                                                                   | `IfNotPresent`                  |
| `service.type`                     | Kubernetes service type for the Logitech Media Server UI/API                                                                        | `ClusterIP`                     |
| `service.clusterIP`                | ClusterIP for Logitech Media Server service; the default (empty string) will cause it to be auto-assigned                           | `""`                            |
| `service.port`                     | Kubernetes port where Logitech Media Server is exposed                                                                              | `9000`                          |
| `service.annotations`              | Service annotations for Logitech Media Server                                                                                       | `{}`                            |
| `service.labels`                   | Custom labels                                                                                                                       | `{}`                            |
| `service.loadBalancerIP`           | Load balancer IP for Logitech Media Server                                                                                          | `{}`                            |
| `service.loadBalancerSourceRanges` | List of IP CIDRs allowed access to load balancer (if supported)                                                                     | None                            |
| `service.externalTrafficPolicy`    | Set the externalTrafficPolicy in the Service to either Cluster or Local                                                             | `Cluster`                       |
| `ingress.enabled`                  | Enables Ingress                                                                                                                     | `false`                         |
| `ingress.annotations`              | Ingress annotations                                                                                                                 | `{}`                            |
| `ingress.labels`                   | Custom labels                                                                                                                       | `{}`                            |
| `ingress.path`                     | Ingress path                                                                                                                        | `/`                             |
| `ingress.hosts`                    | Ingress accepted hostnames                                                                                                          | `logitech-media-server`         |
| `ingress.tls`                      | Ingress TLS configuration                                                                                                           | `[]`                            |
| `settings.timezone`                | Timezone Logitech Media Server should run as, e.g. 'America/New York'                                                               | `UTC`                           |
| `persistence.config.enabled`       | Use persistent volume to store config                                                                                               | `true`                          |
| `persistence.config.size`          | Size of persistent volume claim                                                                                                     | `1Gi`                           |
| `persistence.config.existingClaim` | Use an existing PVC to persist config                                                                                               | `nil`                           |
| `persistence.config.storageClass`  | Type of persistent volume claim                                                                                                     | `-`                             |
| `persistence.config.accessModes`   | Persistence access modes                                                                                                            | `[]`                            |
| `resources`                        | CPU/Memory resource requests/limits                                                                                                 | `{}`                            |
| `nodeSelector`                     | Node labels for pod assignment                                                                                                      | `{}`                            |
| `tolerations`                      | Toleration labels for pod assignment                                                                                                | `[]`                            |
| `affinity`                         | Affinity settings for pod assignment                                                                                                | `{}`                            |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```console
helm install --name my-release \
  --set settings.timezone="America/New York" \
    incubator/logitech-media-server
```

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart. For example,

```console
helm install --name my-release -f values.yaml incubator/logitech-media-server
```

Read through the [values.yaml](values.yaml) file. It has several commented out suggested values.

[home]: https://www.mysqueezebox.com/
[github]: https://github.com/Logitech/slimserver
[docker]: https://hub.docker.com/r/doliana/logitech-media-server/tags

